const mongoose = require('mongoose');
const {Schema} = mongoose;

const schema = new Schema(
    {
        viewCount: {
            type: Number
        },

        likeCount: {
            type: Number
        },

        dislikeCount: {
            type: Number
        },

        commentCount: {
            type: Number
        }
    },
    {
        timestamps: true
    }
);

schema.plugin(require('mongoose-create-or-update'));

schema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Video', schema);
