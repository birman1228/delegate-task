const app = require('./app');
const database = require('./database');
const config = require('./config');
const CronJob = require('cron').CronJob;
const fetch = require("node-fetch");
const videosConfig = require("./videos-config").videos;
const Video = require('./models/video');
Video.id = function (str) { return str; };

const fetchVideosStatistics = () => {
    const ids = videosConfig.map(item => item.id).join(',');
    const url = `https://www.googleapis.com/youtube/v3/videos?part=statistics&id=${ids}&key=${config.YOUTUBE_API_KEY}`;

    return fetch(url)
        .then(res => res.json())
        .then(({items}) => items.map(item => item.statistics))
        .then(statisticsArray => {
            return statisticsArray.map((statistics, index) => {
                const {viewCount, likeCount, dislikeCount, commentCount} = statistics;
                const id = videosConfig[index].mongoId || null;

                return Video.createOrUpdate(
                    {
                    '_id': id
                    },
                    {
                        viewCount,
                        likeCount,
                        dislikeCount,
                        commentCount
                    })
                    .then(data => {
                        videosConfig[index].mongoId = data._id;
                    })
                    .catch(err => {
                        console.log(err);
                    });
            })
        });
}

database()
    .then(info => {
        console.log(`Connected to ${info.host}:${info.port}/${info.name}`);
        app.listen(config.PORT, () =>
            console.log(`Example app listening on port ${config.PORT}!`)
        );
    })
    .then(() => {
        Video.deleteMany({}, () => {
            fetchVideosStatistics();
            setInterval(fetchVideosStatistics, 5000);
        });
    })
    .catch((e) => {
        console.error(e);
        process.exit(1);
    })
