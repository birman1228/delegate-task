const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const Video = require('./models/video');

app.use(express.static(__dirname + "/src"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const withTryCatch = func => {
    return function(req, res) {
        try {
            return func(req, res);
        } catch (e) {
            console.log(e);
        }
    };
};

app.get(
    '/',
    withTryCatch(function(req, res) {
        res.sendFile(__dirname + '/src/index.html');
    })
);

app.get(
    '/api/videos/',
    withTryCatch((req, res) => {
        Video.find({})
            .then(videos => {
                res.send(videos);
            })
            .catch(err => {
                res.status(200).json({ err: err });
            });
    })
);

module.exports = app;
