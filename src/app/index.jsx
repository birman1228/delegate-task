import React from "react";
import { render } from "react-dom";

import ReactTable from 'react-table'


import 'react-table/react-table.css'
import "./../scss/main.scss";

const columns = [
    {
        Header: 'Views',
        accessor: 'viewCount'
    },
    {
        Header: 'Likes',
        accessor: 'likeCount'
    },
    {
        Header: 'Dislikes',
        accessor: 'dislikeCount'
    },
    {
        Header: 'Comments',
        accessor: 'commentCount'
    }
];

class App extends React.Component {
    state = { data: []};

    componentDidMount() {
        this.fetchData();

        const intervalId = setInterval(this.fetchData, 5000);
        this.setState({intervalId: intervalId});
    }

    componentWillUnmount() {
        clearInterval(this.state.intervalId);
    }

    fetchData = () => {
        return fetch('/api/videos')
            .then(res => res.json())
            .then(videos => {
                this.setState({data: videos});
            })
    }

    render() {

        const {data} = this.state;

        const dataTable = data.map(item => {
            const {viewCount, likeCount, dislikeCount, commentCount} = item;

            return ({
                viewCount,
                likeCount,
                dislikeCount,
                commentCount
            });

        });

        return (
            <div className="home">
                <ReactTable
                    data={dataTable}
                    columns={columns}
                />
            </div>
        );
    }
}

render(<App />, document.getElementById("app"));
